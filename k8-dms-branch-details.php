<?php
/*
	Plugin Name: K8 DMS Branch Details
	Plugin URI: http://www.kri8it.com
	Description: Show Branch Details
	Author: Charl Pretorius
	PageLines: true
	Version: 1.0.3
	Section: true
	Class Name: K8BranchDetails
	Filter: component
 	Loading: reload
	
	K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-branch-details/
	
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;

if( ! class_exists( 'K8BranchLocator' ) )
	return;
	
class K8BranchDetails extends PageLinesSection {
	
	function section_persistent(){
        
		
		
    }
		
	function section_template() {
		
		global $post;
							
		$region 	= $this->opt( 'k8_branch_regions', array( 'default' => -1 ) );
		$cols 		= $this->opt( 'k8_branch_cols', array( 'default' => 4 ) );
		$count 		= $this->opt( 'k8_branch_count', array( 'default' => 3 ) );
		$title_wrap = $this->opt( 'k8_branch_details_title_wrap', array( 'default' => 'h3' )  );
		
		if( $region < 0 && is_tax( 'region' ) ) $region = get_queried_object_id();
		
		if( $this->opt( 'k8_branch' ) ):
			
			$branches = get_post( $this->opt( 'k8_branch' ) );
		
		elseif( is_singular( 'branch' ) ):
			
			$branches = array( $post );
			
		else:
			
			$args = array(
				'post_type' => 'branch',
				'posts_per_page' => $count,
				'orderby' => 'menu_order'
			);
			
			if( $region > 0 ):
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'branch',
						'field'    => 'id',
						'terms'    => $region,
					),
				);
			endif; 
			
			$branches = get_posts( $args );
			
		endif;
		
		$branches = apply_filters( 'k8_branch_details_branches', $branches, $region, $count );
		
		if( is_array( $branches ) && ! empty( $branches ) ):
		
			do_action( 'k8_branch_details_before' );
			
			echo '<div class="k8-branch-details">';
			
			$width = 0;
			$count = 1; 
			
			foreach( $branches as $branch ):
				
				$title			= get_the_title( $branch->ID );
				$data 			= get_field( 'branch_location', $branch->ID );
				$contact_number = get_field( 'contact_number', $branch->ID );
				$fax_number 	= get_field( 'fax_number', $branch->ID );
				$email 			= get_field( 'email_contact', $branch->ID );
				$address		= get_field( 'physical_address', $branch->ID );
				$postaladdress	= get_field( 'postal_address', $branch->ID );
				$link 			= get_permalink( $branch->ID );
				
				if($width == 0)	echo '<div class="row fix">';
				
				echo '<div class="k8-branch span' . $cols . ' fix">';
					
					do_action( 'k8_branch_details_item_inside_before' );
					
					printf( '<%s class="branch-head">%s</%s>', $title_wrap, $title, $title_wrap );
					
					echo '<ul>';
					
						echo '<li class="fix contact-number"><label>' . __( 'Contact Number', 'k8' ) . '</label>' . $contact_number . '</li>';
						echo '<li class="fix fax-number"><label>' . __( 'Fax Number', 'k8' ) . '</label>' . $fax_number . '</li>';
						echo '<li class="fix email"><label>' . __( 'Email', 'k8' ) . '</label><a href="mailto:' . $email . '">' . $email . '</a></li>';
						echo '<li class="fix address"><label>' . __( 'Address', 'k8' ) . '</label><p>' . $address . '</p></li>';
						
						if( ! is_singular( 'branch' ) && ! $this->opt( 'k8_branch' ) ) printf( '<a class="btn btn-large btn-link-color" href="%s">%s</a>', $link, __( 'Read More', 'pagelines' ) );
					
					echo '</ul>';
					
					do_action( 'k8_branch_details_item_inside_after' );
				
				echo '</div>';
				
				$width += $cols;

				if($width >= 12 || $count == $boxes){
					$width = 0;
					echo '</div>';
				}
				
			endforeach;	
			
			echo '</div>';
			
			do_action( 'k8_branch_details_after' );
		
		endif;
		
	}

	function section_opts(){
		
		$branch_array = array();
		$branches = get_posts( 'post_type=branch&posts_per_page=-1' );
		foreach( $branches as $branch ):
			$branch_array[$branch->ID] = array( 'name' => $branch->post_title );
		endforeach;		
						
		$opts = array(
			array(
				'type'		=> 'multi',
				'key'		=> 'k8_branch_detail_settings',
				'col'		=> 1,
				'opts'		=> array(
					array(
						'key'			=> 'k8_branch_regions',
						'type' 			=> 'select_taxonomy',
						'post_type'		=> 'branch',
						'label' 		=> __( 'Select from a region', 'pagelines' ),
					),
					array(
						'type' 			=> 'select',
						'key'			=> 'k8_branch_details_title_wrap',
						'label' 		=> __( 'Title Text Wrapper', 'pagelines' ),
						'default'		=> 'h3',
						'opts'			=> array(
							'h1'			=> array('name' => '&lt;h1&gt;'),
							'h2'			=> array('name' => '&lt;h2&gt;  (default)'),
							'h3'			=> array('name' => '&lt;h3&gt;'),
							'h4'			=> array('name' => '&lt;h4&gt;'),
							'h5'			=> array('name' => '&lt;h5&gt;'),
						)
					),
					array(
						'key'			=> 'k8_branch_cols',
						'type' 			=> 'count_select',
						'count_start'	=> 1,
						'count_number'	=> 12,
						'default'		=> '4',
						'label' 	=> __( 'Number of Columns for Each Box (12 Col Grid)', 'pagelines' ),
					),
					array(
						'key'		=> 'k8_branch_count',
						'label'		=> __( 'Count Number', 'pagelines' ),
						'type'		=> 'text_small',
						'default'	=> -1
					),
					array(
						'key'		=> 'k8_branch',
						'label'		=> __( 'Select Specific Branch', 'pagelines' ),
						'type'		=> 'select',
						'opts'		=> $branch_array 
					),													
				)
			)
		);
		return $opts;		
	}
}