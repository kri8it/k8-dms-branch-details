## Changelog

= 1.0 =
* Initial release

= 1.0.1 =
* Added GitHub Updater Support

= 1.0.2 =
* Order branches by menu_order by default

= 1.0.3 =
* Version Fix